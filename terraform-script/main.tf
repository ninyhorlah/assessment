module "eks" {
    source = "terraform-aws-modules/eks/aws"
    version = "~> 18.0"
    cluster_name    = var.cluster_name
    cluster_version = "1.21"

    subnet_ids         = module.vpc.private_subnets
    cluster_endpoint_private_access = true
    vpc_id = module.vpc.vpc_id

    eks_managed_node_groups = {
        green = {
        min_size     = 1
        max_size     = 5
        desired_size = 1

        name           = "shell-node-group"
        instance_types = ["t2.medium"]
        vpc_security_group_ids = [aws_security_group.all_worker_group.id]
        }
    }
}

resource "aws_ecr_repository" "ecr_repo" {
  name                 = "newsily"
  image_tag_mutability = "IMMUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
}

resource "aws_ecr_repository" "ecr_repo2" {
  name                 = "newsily-server"
  image_tag_mutability = "IMMUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
}
