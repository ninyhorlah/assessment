variable "region" {
  description = "region of our application"
  type = string
}

variable "cluster_name" {
  description = "name of the cluster"
  type = string
}