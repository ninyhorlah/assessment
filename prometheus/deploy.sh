#!/bin/bash
kubectl create namespace prometheus

helm repo add prometheus-community https://prometheus-community.github.io/helm-charts

helm install prometheus prometheus-community/prometheus \
    --namespace prometheus \
    --set alertmanager.persistentVolume.storageClass="gp2" \
    --set server.persistentVolume.storageClass="gp2"

#kubectl port-forward -n prometheus deploy/prometheus-server 8080:9090
#kubectl --namespace prometheus port-forward $POD_NAME 9091