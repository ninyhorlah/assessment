# Newsily
> This repository contains a containerized microservice application which uses python(django) for the backend and React.js for the frontend.

### The repository contains a terraform script that:
```
  creates a vpc 
  elastic kubernetes service with managed nodes 
  elastic container registory that holds the containerized image
  kubernetes deployment and service resource
```
### The repository contains an automation script
> Gitlab ci is used for the automation and it builds and push the dockerfile to the container registry created by the terraform script with some predefined rules

### Architectural diagram of the infrastructure
![Architectural diagram of the infrastructure](./asset/diagram.png)