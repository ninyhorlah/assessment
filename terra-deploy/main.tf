

module "grafana_prometheus_monitoring" {
  source = "git::https://github.com/DNXLabs/terraform-aws-eks-grafana-prometheus.git"

  enabled = true
}

resource "kubernetes_deployment" "newsily_fe" {
  metadata {
    name = "newsily-fe"
    namespace = "newsily-fe"
    labels = {
      test = "newsily-fe"
    }
  }

  spec {
    replicas = 2

    selector {
      match_labels = {
        test = "newsily-fe"
      }
    }

    template {
      metadata {
        labels = {
          test = "newsily-fe"
        }
      }

      spec {
        container {
          image = "242040323037.dkr.ecr.us-east-1.amazonaws.com/newsily:43"
          name  = "newsily-fe"
          image_pull_policy = "IfNotPresent"

          port {
           container_port = 4000
           name = "newsily-fe"
           protocol = "TCP"
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "newsily_fe_service" {
  metadata {
    name = "newsily-fe"
    namespace = "newsily-fe"
  }
  spec {
    selector = {
      test = "newsily-fe"
    }
    
    port {
      port = 4000
      target_port = 4000
      protocol = "TCP"
      
    }
    type = "LoadBalancer"
  }
}

resource "kubernetes_service" "newsily_fe_cip" {
  metadata {
    name = "newsily-fe-cip"
    namespace = "newsily-fe"
  }
  spec {
    selector = {
      test = "newsily-fe"
    }
    port {
      port = 4000
      target_port = 4000
      protocol = "TCP"
    }
    type = "ClusterIP"
  }
}

resource "kubernetes_deployment" "newsily_be" {
  metadata {
    name = "newsily-be"
    namespace = "newsily-be"
    labels = {
      test = "newsily-be"
    }
  }

  spec {

    selector {
      match_labels = {
        test = "newsily-be"
      }
    }
    replicas = 2

    template {
      metadata {
        labels = {
          test = "newsily-be"
        }
      }

      spec {
        container {
          image = "242040323037.dkr.ecr.us-east-1.amazonaws.com/newsily-server:46"
          name  = "newsily-be"
          image_pull_policy = "IfNotPresent"

          port {
           container_port = 8000 
           name = "newsily-be"
          }
          
          command = [ "/bin/sh" ]
          args = [ "-c", "python manage.py makemigrations; python manage.py migrate; python manage.py runserver 0.0.0.0:8000" ]
        }
      }
    }
  }
}

resource "kubernetes_service" "newsily_be_server" {
  metadata {
    name = "newsily-be"
    namespace = "newsily-be"
  }
  spec {
    selector = {
      test = "newsily-be"
    }
    port {
      port = 8000
      target_port = 8000
      protocol = "TCP"
    }
    type = "ClusterIP"
  }
}

resource "kubernetes_namespace" "ns-fe" {
  metadata {
    name = "newsily-fe"
  }
}

resource "kubernetes_namespace" "ns-be" {
  metadata {
    name = "newsily-be"
  }
}
